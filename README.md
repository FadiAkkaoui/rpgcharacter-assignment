# RPGcharacter-assignment
This RPG character game, it's developed through a Object Oriented programming pattern with C#/.NET Console Application. With this game you can create characters, equip them with weapons and armors. You can also level them up and display their stats.

## Test and Deploy

install Visual Studio

Git clone: https://gitlab.com/FadiAkkaoui/rpgcharacter-assignment.git

cd rpgcharacter-assignment

Open with visual studio

## Name
RPG-Character

## Description
This RPG character game, it's developed through a Object Oriented programming pattern with C#/.NET Console Application. With this game you can create characters, equip them with weapons and armors. You can also level them up and display their stats.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Contributing
@FadiAkkaoui
