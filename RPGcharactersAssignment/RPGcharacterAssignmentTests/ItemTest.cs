﻿using RPGcharactersAssignment;
using RPGcharactersAssignment.InvalidExceptions;
using RPGcharactersAssignment.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static RPGcharactersAssignment.items.Weapons;

namespace RPGcharacterAssignmentTests
{
    public class ItemTest
    {
        Weapons testAxe = new Weapons(

            "Common Axe",
            1,
            Slot.Weapon,
            Weapon.Axes,
            7,
            1.1
        );

        Armor testPlateBody = new Armor(

            "Common plate body armour",
            1,
            Slot.Body,
            ArmorType.Plate,
            new PrimaryAttribute() { Strength = 1 }
        );

        Weapons testBow = new Weapons(

            "Common bow",
            1,
            Slot.Weapon,
            Weapon.Bows,
            12,
            0.8
        );

        Armor testClothHead = new Armor(

            "Common cloth head armor",
            1,
            Slot.Head,
            ArmorType.Cloth,
            new PrimaryAttribute() { Intelligence = 5}
        );

        [Fact]
        public void EquipWeaponHighLevel_Character_ShouldThrowInvalidWeaponExeption()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Weapons weapons = testAxe;
            testAxe.RequiredLevel = 2;
            string expected = "Wrong weapon type or not requierd";

            //Act
            Exception exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(weapons, Slot.Weapon));
            string actual = exception.Message;

            //Assert
            Assert.Equal(expected,actual);
        }

        [Fact]
        public void EquipArmorHighLevel_Character_ShouldThrowInvalidArmorExeption()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Armor armor = testPlateBody;
            testPlateBody.RequiredLevel = 2;
            string expected = "Wrong armor type or not requierd";

            //Act
            Exception exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(armor, Slot.Body));
            string actual = exception.Message;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWrongWeapon_Character_ShouldThrowInvalidWeaponExeption()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Weapons weapon = testBow;
            string expected = "Wrong weapon type or not requierd";

            //Act
            Exception exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(weapon, Slot.Weapon));
            string actual = exception.Message;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipWrongArmor_Character_ShouldThrowInvalidArmorExeption()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Armor armor = testClothHead;
            string expected = "Wrong armor type or not requierd";

            //Act
            Exception exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(armor, Slot.Head));
            string actual = exception.Message;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipValidWeapon_Character_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Weapons weapon = testAxe;
            string expected = "New weapon equipped!";

            //Act
            string actual = warrior.EquipWeapon(weapon, Slot.Weapon);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipValidArmor_Character_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Armor armor = testPlateBody;
            string expected = "New armor equipped!";

            //Act
            string actual = warrior.EquipArmor(armor, Slot.Body);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamageIfNoWeaponEquipped_Character_ShouldReturnDamage()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            double expected = 1 * (1 + (5 / 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void CalculateDamageWithWeaponEquipped_Character_ShouldReturnDamage()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Weapons axe = testAxe;
            double expected = (7 * 1.1) * (1 + (5 * 0.01));

            //Act
            warrior.EquipCharacter(axe, Slot.Weapon);
            double actual = warrior.Damage();
            
            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void CalculateDamageWithWeaponEquippedAndArmorEquipped_Character_ShouldReturnDamage()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Weapons axe = testAxe;
            Armor armor = testPlateBody;
            double expected = (7 * 1.1) * (1 + ((5+1) * 0.01));

            //Act
            warrior.EquipCharacter(axe, Slot.Weapon);
            warrior.EquipCharacter(armor, Slot.Body);
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(actual, expected);
        }


    }
}
