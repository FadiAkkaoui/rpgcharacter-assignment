﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGcharactersAssignment;
using RPGcharactersAssignment.items;

namespace RPGcharacterAssignmentTests
{
    public class CharacterTest
    {
         
        [Fact]
        public void Create_MageCharacter_ShouldBeLevel1()
        {
            //Arrange
            int[] expected = new int[] {1,1,8,1};
            //Act
            Mage mage = new Mage("Mage");
            //Assert
            Assert.Equal(expected, new int[] { mage.Stats.Strength, mage.Stats.Dexterity, mage.Stats.Intelligence, mage.Level});
        }
        [Fact]
        public void Create_MageCharacter_ShouldBeLevel2()
        {
            //Arrange
            int[] expected = new int[] { 2, 2, 13, 2 };
            //Act
            Mage mage = new Mage("Mage");
            mage.LevelUp();
            //Assert
            Assert.Equal(expected, new int[] { mage.Stats.Strength, mage.Stats.Dexterity, mage.Stats.Intelligence, mage.Level });
        }
        [Fact]
        public void Create_RogueCharacter_ShouldBeLevel1()
        {
            //Arrange
            int[] expected = new int[] { 2, 6, 1, 1 };
            //Act
            Rogue rogue = new Rogue("Rogue");
            //Assert
            Assert.Equal(expected, new int[] { rogue.Stats.Strength, rogue.Stats.Dexterity, rogue.Stats.Intelligence, rogue.Level });
        }
        [Fact]
        public void Create_RogueCharacter_ShouldBeLevel2()
        {
            //Arrange
            int[] expected = new int[] { 3, 10, 2, 2 };
            //Act
            Rogue rogue = new Rogue("Rogue");
            rogue.LevelUp();
            //Assert
            Assert.Equal(expected, new int[] { rogue.Stats.Strength, rogue.Stats.Dexterity, rogue.Stats.Intelligence, rogue.Level });
        }
        [Fact]
        public void Create_RangerCharacter_ShouldBeLevel1()
        {
            //Arrange
            int[] expected = new int[] { 1, 7, 1, 1 };
            //Act
            Ranger ranger = new Ranger("Ranger");
            //Assert
            Assert.Equal(expected, new int[] { ranger.Stats.Strength, ranger.Stats.Dexterity, ranger.Stats.Intelligence, ranger.Level });
        }
        [Fact]
        public void When_RangerCharacter_LevelUP_ShouldBeLevel2()
        {
            //Arrange
            int[] expected = new int[] { 2, 12, 2, 2 };
            //Act
            Ranger ranger = new Ranger("Ranger");
            ranger.LevelUp();
            //Assert
            Assert.Equal(expected, new int[] { ranger.Stats.Strength, ranger.Stats.Dexterity, ranger.Stats.Intelligence, ranger.Level });
        }
        [Fact]
        public void When_WarriorCharacter_LevelUP_ShouldBeLevel1()
        {
            //Arrange
            int[] expected = new int[] { 5, 2, 1, 1 };
            //Act
            Warrior warrior = new Warrior("Warrior");
            //Assert
            Assert.Equal(expected, new int[] { warrior.Stats.Strength, warrior.Stats.Dexterity, warrior.Stats.Intelligence, warrior.Level });
        }
        [Fact]
        public void When_WarriorCharacter_LevelUP_ShouldBeLevel2()
        {
            //Arrange
            int[] expected = new int[] { 8, 4, 2, 2 };
            //Act
            Warrior warrior = new Warrior("Warrior");
            warrior.LevelUp();
            //Assert
            Assert.Equal(expected, new int[] { warrior.Stats.Strength, warrior.Stats.Dexterity, warrior.Stats.Intelligence, warrior.Level });
        }
    }
}
