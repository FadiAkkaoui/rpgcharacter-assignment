﻿using RPGcharactersAssignment.items;
using System;
using System.Text;
using System.Collections.Generic;
using static RPGcharactersAssignment.items.Weapons;

namespace RPGcharactersAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new Warrior("Warrior");
            Weapons testAxe = new Weapons(

            "Common Axe",
            1,
            Slot.Weapon,
            Weapon.Axes,
            7,
            1.1
        );

            warrior.EquipWeapon(testAxe, Slot.Weapon);
            Console.WriteLine(warrior.CharacterStatsDisplay());
            warrior.LevelUp();
            Console.WriteLine('\b');
            Console.WriteLine(warrior.CharacterStatsDisplay());
        }
    }
}
