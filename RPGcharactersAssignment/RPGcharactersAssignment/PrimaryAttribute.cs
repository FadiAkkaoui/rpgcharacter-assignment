﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment
{
    public class PrimaryAttribute
    {
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Strength { get; set; }
    }
}
