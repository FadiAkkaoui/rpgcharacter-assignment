﻿using System;
using System.Collections.Generic;
using RPGcharactersAssignment.items;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment
{
   public class Rogue : Character 
    {
        public static Weapons.Weapon[] AllowedWepons = { Weapons.Weapon.Daggers, Weapons.Weapon.Swords };
        public static ArmorType[] AllowedArmors = { ArmorType.Leather, ArmorType.Mail };
        public Rogue(string name) : base(name, 2, 6, 1, 1, 4, 1, AllowedWepons, AllowedArmors){}
        public override double Damage()
        {
            PrimaryAttribute total = GetAllAttributes();
            double damage = 0;
            foreach (KeyValuePair<Slot, Item> element in Equipment)
            {
                if (element.Value is Armor)
                {
                    Armor armor = (Armor)element.Value;
                    total.Strength += armor.ArmorStats.Strength;
                }
            }

            if (Equipment[Slot.Weapon] != null)
            {
                Weapons weapons = (Weapons)Equipment[Slot.Weapon];

                damage += weapons.DPS() * (1 + (total.Strength * 0.01));
                return damage;
            }else
            {
                damage += 1 * (1 + (total.Strength / 0.01));
                return damage;
            }
        }

        /// <summary>
        /// This method equips the weapon for the character
        /// </summary>
        public override bool EquipWeaponCharacter(Weapons weapons)
        {
            if (weapons.WeaponType == Weapons.Weapon.Daggers || weapons.WeaponType == Weapons.Weapon.Swords)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method equips the Armor for the character
        /// </summary>
        public override bool EquipArmorCharacter(Armor armor)
        {
            if (armor.ArmorType == ArmorType.Leather || armor.ArmorType == ArmorType.Mail)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
