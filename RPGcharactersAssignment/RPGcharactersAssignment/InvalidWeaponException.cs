﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment.InvalidExceptions
{
    public class InvalidWeaponException : Exception
    {
        public override string Message => "Wrong weapon type or not requierd";
    }
}
