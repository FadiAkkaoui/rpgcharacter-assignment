﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment.InvalidExceptions
{
    public class InvalidArmorException : Exception
    {
        public override string Message => "Wrong armor type or not requierd";
    }
}
