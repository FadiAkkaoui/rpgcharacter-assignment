﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment.items
{
    public abstract class Item 
    {
        public Slot EquibableSlot;
        public Slot ItemSlot { get; set; }
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Item(string name, int lvlReq, Slot slot)
        {
            Name = name;
            RequiredLevel = lvlReq;
            ItemSlot = slot;
        }

        /// <summary>
        /// This method gives access to the primary attribute 
        /// </summary>
        /// <returns>returns stats</returns>
        public abstract PrimaryAttribute GetAttribute();
    }
}
