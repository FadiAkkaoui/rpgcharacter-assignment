﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment.items
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttribute ArmorStats = new();
        public Armor(string name, int lvlReq, Slot slot, ArmorType armorType, PrimaryAttribute primaryAttribute) : base (name, lvlReq, slot)
        {
            ArmorStats = primaryAttribute;
            ArmorType = armorType;
        }
        public override PrimaryAttribute GetAttribute()
        {
            return ArmorStats;
        }
    }
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
