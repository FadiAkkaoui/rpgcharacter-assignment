﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment.items
{
   public class Weapons : Item
    {
        public Weapon WeaponType { get; set; }
        public WeaponsAttribute WeaponStats = new();
        public Weapons(string name, int lvlReq, Slot slot, Weapon type, double damage, double attackspeed) : base (name, lvlReq, slot)
        { 
            WeaponType = type;
            WeaponStats.AttackSpeed = attackspeed;
            WeaponStats.Damage = damage;
        }
        public enum Weapon
        {
            Axes,
            Bows,
            Daggers,
            Hammers,
            Staffs,
            Swords,
            Wands

        }
        /// <summary>
        /// Calculates Damage per second and returns the value
        /// </summary>
        /// <returns>the calulation for damage per second</returns>
        public double DPS()
        {
            double dps = WeaponStats.Damage * WeaponStats.AttackSpeed;
            return dps;
        }
        public override PrimaryAttribute GetAttribute()
        {
            return WeaponStats;
        }
}
}
