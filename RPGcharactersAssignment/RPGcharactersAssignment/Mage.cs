﻿using RPGcharactersAssignment.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment
{
     public class Mage : Character 
    {
        public static Weapons.Weapon[] AllowedWepons = { Weapons.Weapon.Staffs, Weapons.Weapon.Wands };
        public static ArmorType[] AllowedArmors = { ArmorType.Cloth };
        public Mage(string name) : base(name, 1,1,8, 1,1,5, AllowedWepons, AllowedArmors){}
        public override double Damage()
        {
            PrimaryAttribute total = GetAllAttributes();

            double damage = 0;
            foreach (KeyValuePair<Slot, Item> element in Equipment)
            {
                if (element.Value is Armor)
                {
                    Armor armor = (Armor)element.Value;
                    total.Strength += armor.ArmorStats.Strength;
                }
            }

            if (Equipment[Slot.Weapon] != null)
            {
                Weapons weapons = (Weapons)Equipment[Slot.Weapon];

                damage += weapons.DPS() * (1 + (total.Strength * 0.01));
                return damage;
            }else
            {
                damage += 1 * (1 + (total.Strength / 0.01));
                return damage;
            }
        }
        public override bool EquipWeaponCharacter(Weapons weapons)
        {
            if(weapons.WeaponType == Weapons.Weapon.Staffs && weapons.ItemSlot == Slot.Weapon|| weapons.WeaponType == Weapons.Weapon.Wands && weapons.ItemSlot == Slot.Weapon)
            {
                return true;
            }else
            {
                return false;
            }
        }
        public override bool EquipArmorCharacter(Armor armor)
        {
            if(armor.ArmorType == ArmorType.Cloth || armor.ItemSlot != Slot.Weapon)
            {
                return true;
            }else
            {
                return false;
            }
        }
    }
}
