﻿using System;
using System.Collections.Generic;
using RPGcharactersAssignment.items;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment
{
    public class Warrior : Character 
    {
        public static Weapons.Weapon[] AllowedWepons = { Weapons.Weapon.Axes, Weapons.Weapon.Hammers, Weapons.Weapon.Swords };
        public static ArmorType[] AllowedArmors = { ArmorType.Mail, ArmorType.Plate };
        public Warrior(string name) : base(name, 5, 2, 1, 3, 2, 1, AllowedWepons, AllowedArmors){}
        public override double Damage()
        {
            PrimaryAttribute total = GetAllAttributes();
          
            double damage = 0;
            foreach (KeyValuePair<Slot, Item> element in Equipment)
            {
                if (element.Value is Armor)
                {
                    Armor armor = (Armor)element.Value;
                    total.Strength += armor.ArmorStats.Strength;
                }
            }

            if (Equipment[Slot.Weapon] != null)
            {
                Weapons weapons = (Weapons)Equipment[Slot.Weapon];

                damage += weapons.DPS() * (1 + (total.Strength * 0.01));
                return damage;
            }else
            {
                damage += 1 * (1 + (total.Strength / 0.01));
                return damage;
            }
        }
        public override bool EquipWeaponCharacter(Weapons weapons)
        {
            if (weapons.WeaponType == Weapons.Weapon.Axes || weapons.WeaponType == Weapons.Weapon.Hammers || weapons.WeaponType== Weapons.Weapon.Hammers)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool EquipArmorCharacter(Armor armor)
        {
            if (armor.ArmorType == ArmorType.Mail || armor.ArmorType == ArmorType.Plate)
            {
                return true;
            }
            else
            {
                return false;
                
            }
        }
    }
}
