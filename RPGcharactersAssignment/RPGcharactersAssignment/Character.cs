﻿using RPGcharactersAssignment.InvalidExceptions;
using RPGcharactersAssignment.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public PrimaryAttribute Stats = new();
        public PrimaryAttribute CharacterLevelUp = new();
        public Weapons.Weapon[] AllowedWepons { get; set; }
        public ArmorType[] AllowedArmors { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; } = new Dictionary<Slot, Item>()
        {
            {Slot.Head, null},
            {Slot.Body, null},
            {Slot.Legs, null},
            {Slot.Weapon, null}
        };
        public Character(string name, int stength, int dexterity, int intelligence, int levelUpStrength, int levelUpDexterity, int levelUpIntelligance, Weapons.Weapon[] allowedWeapons, ArmorType[] armorType)
        {
            Name = name;

            Stats.Strength = stength;
            Stats.Dexterity = dexterity;
            Stats.Intelligence = intelligence;

            CharacterLevelUp.Strength = levelUpStrength;
            CharacterLevelUp.Dexterity = levelUpDexterity;
            CharacterLevelUp.Intelligence = levelUpIntelligance;

            AllowedWepons = allowedWeapons;
            AllowedArmors = armorType;

        }

        /// <summary>
        /// This method levels up the character, depending on which character it will add to their stats
        /// </summary>
        /// <returns>Retuns Level</returns>
        public int LevelUp()
        {
            Stats.Strength += CharacterLevelUp.Strength;
            Stats.Dexterity += CharacterLevelUp.Dexterity;
            Stats.Intelligence += CharacterLevelUp.Intelligence;
            Level++;

            return Level;
        }

        /// <summary>
        /// This method levels up the character, depending on which character it will add to their stats
        /// </summary>
        /// <returns>Returns damage of current character</returns>
        public abstract double Damage();

        /// <summary>
        /// This method gets all the attrubutes from primary attibute
        /// </summary>
        /// <returns>Returns the stats of all attributes</returns>
        public virtual PrimaryAttribute GetAllAttributes()
        {
            return Stats;
        }

        /// <summary>
        /// This method takes weapon from EquipWeapon and checks if character can equip the weapon 
        /// </summary>
        /// <returns> true or false</returns>
        public abstract bool EquipWeaponCharacter(Weapons weapons);

        /// <summary>
        /// This method takes armor from EquipArmor and checks if character can equip the armor 
        /// </summary>
        /// <returns> true or false</returns>
        public abstract bool EquipArmorCharacter(Armor armor);

        /// <summary>
        /// This method equips the armor and checks if the character are able to equip it through checking the level requirements
        /// </summary>
        /// <returns>"New armor equipped" if requierments are fulfilled, if not it will throw new InvalidArmorException</returns>
        public virtual string EquipArmor(Armor armor, Slot slot)
        {
            Slot item = armor.ItemSlot;
            if (EquipArmorCharacter(armor) && armor.RequiredLevel <= this.Level)
            {
                Equipment[item] = armor;
                return "New armor equipped!";
            }else
            {
                throw new InvalidArmorException();
            }
        }

        /// <summary>
        /// This method equips the weapon and checks if the character are able to equip it through checking the level requirements
        /// </summary>
        /// <returns>"New weapon equipped" if requierments are fulfilled, if not it will throw new InvalidWeaponException</returns>
        public virtual string EquipWeapon(Weapons weapons, Slot slot)
        {
            //Slot item = weapons.ItemSlot;
            if (EquipWeaponCharacter(weapons) && weapons.RequiredLevel <= this.Level)
            {
                Equipment[Slot.Weapon] = weapons;
                return "New weapon equipped!";
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        /// <summary>
        /// This method checks if user equips an armor or weapon, depending on which one it will equip to the right slot.
        /// </summary>
        /// <returns>eqipped weapon or armor</returns>
        public string EquipCharacter(Item item,  Slot slot)
        {
            if( item is Weapons)
            {
                Weapons weapons = (Weapons)item;
                return EquipWeapon(weapons, slot);
            }
            else
            {
                Armor armor = (Armor)item;
                return EquipArmor(armor, slot);
            }
        }

        /// <summary>
        /// This method displays the stats for character calling the method
        /// </summary>
        /// <returns>stats data displayed </returns>
        public virtual string CharacterStatsDisplay()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Name: " + Name + '\n');
            sb.Append("Level: " + Level.ToString() + '\n');
            sb.Append("Strength: " + Stats.Strength.ToString() + '\n');
            sb.Append("Dexterity: " + Stats.Dexterity.ToString() + '\n');
            sb.Append("Intelligence: " + Stats.Intelligence.ToString() + '\n');
            sb.Append("Damage: " + Damage() + '\n');
            sb.Append("Statstotal: " + (this.GetAllAttributes().Strength + this.GetAllAttributes().Dexterity + this.GetAllAttributes().Intelligence).ToString());

            return sb.ToString();
        }

    }
}
