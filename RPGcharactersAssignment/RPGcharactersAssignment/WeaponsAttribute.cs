﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGcharactersAssignment.items
{
    public class WeaponsAttribute : PrimaryAttribute
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
    }
}
